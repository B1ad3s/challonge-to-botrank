import argparse
from datetime import datetime
import os
from os import path
from requests import request
import yaml


# Initialize the passed arguments if the command was run via command line
def initialize_arguments():
    # Grab command line arguments
    script_dir = os.path.dirname(os.path.realpath(__file__))
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str, default=f"{script_dir}/config.yaml")
    parser.add_argument("-o", "--output", type=str, default=f"{script_dir}/outputs.d")
    parser.add_argument("-i", "--input_file", type=str, action="append")
    parser.add_argument("-t", "--tournament", type=str, action="append")
    parser.add_argument("-w", "--weight_class", type=str, default='3')
    parser.add_argument("-f", "--forefit_string", type=str, action="append")
    args = parser.parse_args()

    config_file = args.config
    output_dir = args.output
    input_files = args.input_file
    tournaments = args.tournament
    weight_class = args.weight_class
    forefit_strings = args.forefit_string

    # Create output_dir if doesn't exist
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Parse config file
    if not (input_files or tournaments):
        parser.error("Must pass in at least one input file or tournament to process")

    try:
        with open(config_file, 'r') as f:
            config_dict = yaml.load(f, Loader=yaml.FullLoader)
    except FileNotFoundError:
        raise Exception("Config file not found, can not continue.")
    except yaml.scanner.ScannerError:
        raise Exception("Could not parse config file as yaml")

    required_keys = ["api_token", "username"]
    for key in required_keys:
        if key not in config_dict:
            raise Exception(f"`{key}` key not in the passed config yaml. Can not continue.")

    api_token = config_dict['api_token']
    username = config_dict['username']

    # Get set of all tournaments to be processed in the current run
    target_tournaments = set()
    if input_files:
        for input_file in input_files:
            if not path.exists(input_file):
                print(f"File `{input_file}` not found. Skipping.")
                continue
            with open(input_file, 'r') as f:
                tournaments_from_file = f.readlines()
                for tournament in tournaments_from_file:
                    tournament = tournament.strip().replace("https://challonge.com/", "")
                    if tournament != "" and tournament[0] != "#":
                        target_tournaments.add(tournament)
    if tournaments:
        for tournament in tournaments:
            target_tournaments.add(tournament.strip().replace("https://challonge.com/", ""))
        if len(target_tournaments) == 0:
            raise Exception("No Challonge tournaments found. Can not continue.")

    return target_tournaments, output_dir, api_token, username, weight_class, forefit_strings


# Main function to convert all passed tournaments into the BotRank format and save to the output dir
def challonge_to_botrank(tournaments, output_dir, api_token, username, weight_class, forefit_strings):
    for tournament in tournaments:
        tournament_info = get_tournament_info(tournament, api_token, username)
        if tournament_info is None:
            continue
        formatted_tournament = process_tournament(tournament_info, weight_class, tournament, forefit_strings)
        if formatted_tournament is None:
            continue
        try:
            output_file = f"{output_dir}/{tournament}.txt"
            with open(output_file, 'w+') as f:
                f.write(formatted_tournament)
        except Exception as ex:
            print(f"{ex.__class__}: Error writing to output for `{tournament}`. Skipping")


# Pulls the information for the passed tournament from Challonge
def get_tournament_info(tournament, api_token, username):
    tournament_url = f"https://api.challonge.com/v1/tournaments/{tournament}.json"
    params = {
            'include_participants': 1,
            'include_matches': 1,
        }

    try:
        r = request(
            "GET",
            tournament_url,
            headers={"User-Agent": "challonge_to_botrank"},
            auth=(username, api_token),
            params=params
        )
    except Exception as ex:
        print(f"{ex.__class__}: Could not pull tournament info, skipping")
        return None
    tournament_info = r.json()
    if "tournament" in tournament_info:
        tournament_info = tournament_info["tournament"]
    else:
        print(f"`tournament` key not in result for {tournament}. Skipping")
        return None

    return tournament_info


# Parse the Challonge tournament results into the BotRank string format
def process_tournament(tournament_info, weight_class, tournament, forefit_strings):
    required_keys = ["name", "started_at", "matches", "participants"]
    for key in required_keys:
        if key not in tournament_info or tournament_info[key] is None:
            print(f'`{key}` not in the returned data for tournament `{tournament}`. Skipping.')
            return None

    botrank_formatted_string = f'{tournament_info["name"]}'

    started_at = datetime.strptime(tournament_info["started_at"], '%Y-%m-%dT%H:%M:%S.%f%z')
    botrank_formatted_string += f'\n{started_at.strftime("%Y-%m-%d")}'

    # Create dict of participant ID to names
    participants = tournament_info["participants"]
    participant_dict = {}
    for participant in participants:
        participant = participant['participant']
        participant_dict[participant['id']] = participant['name']

    matches = tournament_info["matches"]
    for match in matches:
        match = match["match"]
        # Skip match if winner not listed
        if match['winner_id'] is None:
            continue
        winner = participant_dict[match['winner_id']]
        loser = participant_dict[match['loser_id']]

        participant_forfeited = False
        # Ignore the match if someone forfeited as they didn't fight
        if forefit_strings is not None:
            for forefit_string in forefit_strings:
                forefit_string = forefit_string.lower()
                if forefit_string in winner.lower() or forefit_string in loser.lower():
                    participant_forfeited = True
                    break
        if not participant_forfeited:
            botrank_formatted_string += f'\n{weight_class},{winner},{loser}'

    return botrank_formatted_string


# Call everything started from command line
if __name__ == "__main__":
    challonge_to_botrank(*initialize_arguments())
