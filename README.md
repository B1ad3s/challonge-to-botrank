Challonge To BotRank
====================

Description
-----------
The purpose of this script is to allow for quick collection of data from a Challonge tournament and output it in the 
format required to upload to BotRank.

Requirements
----------
Requirements for this script are 
* Python >= 3.7
* The packages in the requirements.txt file
* A config file containing your Challonge username and API key (example in `example_config.yaml`)

How to Run
----------
* This script can either be run on individual Challonge tournaments or have files passed in, each listing multiple 
tournaments
* For the purposes of this script, each weight class you are trying to pull should be split into separate runs
* Parameters the script takes are as below
  - -c / --config / default=f"{script_dir}/config.yaml" --- The path for the config file containing your credentials
  - -o / --output / default=f"{script_dir}/outputs.d" --- The path of the output directory where files should be dumped
  - -w / --weight_class / default="3" --- What will be populated for the BotRank "weight" field for the run
  - -i / --input_file / action="append" --- The input file to pull from. Multiple are allowed.
  - -t / --tournament / action="append" --- The specific tournament to pull from. Multiple are allowed
  - -f / --forefit_string / action="append" --- String values that if seen in a participants name, will cause them to be
  ignored.
* An example run of the script can be seen below
  - `python challonge_to_botrank.py  -i ./example_tournament_file.txt -t fallfury19 -f "(will for"` 
    * This will output the Bot Rank format for all tournaments listed in the example_tournament_file as well as the 
    https://challonge.com/fallfury19 tournament to wherever you have put the challonge_to_botrank.py file, in a 
    a directory `outputs.d` and will mark each of these tournaments as fighting 3lb bots. Because -f `"(will for"` was 
    passed, the code will ignore any fight where a competitor had `"(will for"` included in their name (such as 
    `Saw Murai (will forefit)`), case insensitive.
* Tournaments are de-duplicated by name and so it is OK if you have the same tournament in multiple files, or a file 
and passed in directly 